'use strict'

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://127.0.0.1:8080',
		'./src/activity'
	],
	output: {
		path: './build',
		filename: 'app.js',
		hash: true
	},
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		}]
	},
	plugins: [new HtmlWebpackPlugin({
		template: './src/index.html'
	})]
};
