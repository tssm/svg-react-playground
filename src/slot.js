'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

export class Slot extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return(
			<rect
				height={this.props.height}
				width={this.props.width}
				x={this.props.x}
				y={this.props.y}
				fill="transparent"
				stroke={this.props.stroke}>
				{this.props.children}
			</rect>
		);
	}
}
