'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

export class Card extends React.Component {
	constructor(props) {
		super(props);

		this.handleGrab = this.handleGrab.bind(this);
	}

	handleGrab(e) {
		this.props.onGrab({
			element: e.target,
			cursorX: e.clientX,
			cursorY: e.clientY,
			matrix: e.target
				.getAttributeNS(null, 'transform')
				.slice(7, -1)
				.split(' ')
				.map(element => parseFloat(element))
		});
	}

	render() {
		return (
			<rect
				height={this.props.height}
				width={this.props.width}
				x={this.props.x}
				y={this.props.y}
				fill={this.props.fill}
				transform="matrix(1 0 0 1 0 0)"
				onMouseDown={this.handleGrab}>
				{this.props.children}
			</rect>
		);
	}
}
