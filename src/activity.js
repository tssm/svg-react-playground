'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import {Card} from './card';
import {Slot} from './slot';

class Activity extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			grabbed: null,
			slots: [{
				x: 125,
				y: 125
			}]
		};

		this.handleChildGrab = this.handleChildGrab.bind(this);
		this.handleChildDrag = this.handleChildDrag.bind(this);
		this.handleChildDrop = this.handleChildDrop.bind(this);
	}

	handleChildGrab(child) {
		this.state.grabbed = child;
	};

	handleChildDrag(e) {
		if (this.state.grabbed) {
			const grabbed = this.state.grabbed;
			const dx = e.clientX - grabbed.cursorX;
			const dy = e.clientY - grabbed.cursorY;
			// Editando el estado directamente... ¿Cual es la buena forma de hacerlo?
			grabbed.matrix[4] += dx;
			grabbed.matrix[5] += dy;
			grabbed.element.setAttributeNS(
				null,
				'transform',
				`matrix(${grabbed.matrix.join(' ')})`
			);
			grabbed.cursorX = e.clientX;
			grabbed.cursorY = e.clientY;
		}
	}

	handleChildDrop(e) {
		const grabbedElement = this.state.grabbed.element;
		const boundingClientRect = grabbedElement.getBoundingClientRect();
		const centerX = boundingClientRect.left + 25;
		const centerY = boundingClientRect.top + 25;
		this.state.slots.some(slot => {
			if (centerX > slot.x
				&& centerX < slot.x + 50
				&& centerY > slot.y
				&& centerY < slot.y + 50) {
				const bBox = grabbedElement.getBBox();
				const matrix = this.state.grabbed.matrix;
				matrix[4] = slot.x - bBox.x;
				matrix[5] = slot.y - bBox.y;
				grabbedElement.setAttributeNS(
					null,
					'transform',
					`matrix(${matrix.join(' ')})`
				);
				return true;
			}

			grabbedElement.setAttributeNS(
				null,
				'transform',
				'matrix(1 0 0 1 0 0)'
			);

			return false;
		});

		this.state.grabbed = null;
	}

	render() {
		const slots = this.state.slots.map((slot, i) => {
			return (
				<Slot height="50" width="50" x={slot.x} y={slot.y} stroke="red" key={i}/>
			);
		});
		return (
			<svg style={{
				width: 100 + '%',
				height: 100 + '%',
				position: 'fixed',
				top: 0,
				left: 0,
				bottom: 0,
				right: 0}}
				onMouseMove={this.handleChildDrag}
				onMouseUp={this.handleChildDrop}>
				{slots}
				<Card
					height="50" width="50" x="25" y="25"
					fill="#ff0099"
					onGrab={this.handleChildGrab} />
			</svg>
		);
	}
}

ReactDOM.render(
	<Activity />,
	document.getElementById('svg')
);
